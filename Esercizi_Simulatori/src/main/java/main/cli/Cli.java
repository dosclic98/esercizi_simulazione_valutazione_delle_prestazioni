package main.cli;

import main.cli.subcommands.Start;
import picocli.CommandLine;


@CommandLine.Command(
        name = "sim",
        subcommands = {
                Start.class
        }
)
public class Cli implements Runnable{
    public static void main(String[] args) {
        int exitCode = new CommandLine(new Cli()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public void run() {

    }
}
