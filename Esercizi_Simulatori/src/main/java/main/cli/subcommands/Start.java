package main.cli.subcommands;

import SimUtils.*;
import main.Sim1;
import main.Sim2;
import main.Sim3;
import picocli.CommandLine;

import java.io.FileNotFoundException;
import java.util.ArrayList;


@CommandLine.Command(name = "start")
public class Start implements Runnable{

    @CommandLine.Option(
            names = {"--cfg", "-config-file"},
            required = true,
            description = "Configuration file name to use (JSON)"
    )
    String configFileName;

    @CommandLine.Option(
            names = {"--m", "-model"},
            required = true,
            description = "Select the model you want to simulate (1, 2 or 3)"
    )
    Integer modelNum;

    @Override
    public void run() {
        System.out.println("Starting sim");
        try {
            if(modelNum == 1) {
                SimConfigM1M2 simConfig = new ConfigReaderM1M2(configFileName).readConfig();
                ArrayList<Sim1> sims = new ArrayList<>();
                ArrayList<SimResultsM1> simResults = new ArrayList<>();
                for(int i = 0; i < simConfig.runs; i++) {
                    System.out.println("\n######## Run " + (i+1) + " ###########\n");
                    sims.add(new Sim1(simConfig.arrivalDist, simConfig.serviceDist, i % 128, simConfig.customers.intValue(), simConfig.servers.intValue()));
                    simResults.add(sims.get(sims.size()-1).runSim());
                }
                new SimOutAnalysisM1(simResults, simConfig.confidence, simConfig.runs).doAnalysis();
            } else if (modelNum == 2) {
                SimConfigM1M2 simConfig = new ConfigReaderM1M2(configFileName).readConfig();
                ArrayList<Sim2> sims = new ArrayList<>();
                ArrayList<SimResultsM2> simResults = new ArrayList<>();
                for(int i = 0; i < simConfig.runs; i++) {
                    System.out.println("\n######## Run " + (i+1) + " ###########\n");
                    sims.add(new Sim2(simConfig.arrivalDist, simConfig.serviceDist, i % 128, simConfig.customers.intValue(), simConfig.servers.intValue()));
                    simResults.add(sims.get(sims.size()-1).runSim());
                }
                new SimOutAnalysisM2(simResults, simConfig.confidence, simConfig.runs).doAnalysis();
            } else if (modelNum == 3){
                SimConfigM3 simConfig = new ConfigReaderM3(configFileName).readConfig();
                ArrayList<Sim3> sims = new ArrayList<>();
                ArrayList<SimResultsM3> simResults = new ArrayList<>();
                for(int i = 0; i < simConfig.runs; i++) {
                    System.out.println("\n######## Run " + (i+1) + " ###########\n");
                    sims.add(new Sim3(simConfig.arrivalDist, simConfig.serviceDist, i % 128,
                            simConfig.customers.intValue(), simConfig.servers.intValue(), simConfig.p, simConfig.seed));
                    simResults.add(sims.get(sims.size()-1).runSim());
                }
                new SimOutAnalysisM3(simResults, simConfig.confidence, simConfig.runs).doAnalysis();
            } else {
                throw new IllegalArgumentException("Select one of the three models available (1,2 or 3)");
            }
        } catch (IllegalArgumentException | IllegalStateException e) {
            System.err.println(e.getMessage());
        } catch (ClassCastException e) {
            System.err.println("Check the type of the parameters you set");
        } catch (FileNotFoundException e) {
            System.err.println("Configuration file not found, please specify the name of an existing configuration file in the 'Config' folder");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
