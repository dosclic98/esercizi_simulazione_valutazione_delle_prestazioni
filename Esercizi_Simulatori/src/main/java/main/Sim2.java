package main;

import SimUtils.*;
import distributions.Distribution;

import java.util.Arrays;


public class Sim2 {

    // Class Sim variables
    public double Clock, SumDelayTime, SumServiceTime, LastEventTime,
            SumResponseTime, OccServersAcc;
    public long QueueLength, NumberInService, MaxQueueLength, CustomersInStationAcc, TotCustomers,
            CurrCustomersInStation, NumberOfDepartures, TotArrivals, NumberOfServers, CliQueueAcc;

    // Integral accumulator for servers utilization
    public double[] serversBusyAcc;

    public int streamIndex;

    // Event types
    public final int arrival = 1;
    public final int departure = 2;

    public EventList FutureEventList;
    public Queue Customers;

    public Distribution distDelays;
    public Distribution distServices;

    public Sim2(Distribution distDelay, Distribution distServ, int streamIndex, int customersInStation, int numServers) {
        CurrCustomersInStation = TotCustomers = customersInStation;
        distDelays = distDelay;
        distServices = distServ;
        NumberOfServers = numServers;
        this.streamIndex = streamIndex;
    }

    public SimResultsM2 runSim() {
        FutureEventList = new EventList();
        Customers = new Queue();

        Initialization();

        // Loop until first "initialCustomersInStation * 500" customers have departed
        long initialCustomersInStation = CurrCustomersInStation;
        while(NumberOfDepartures < initialCustomersInStation * 500) {
            Event evt = FutureEventList.getMin();  // get imminent event
            FutureEventList.dequeue();                    // be rid of it
            Clock = evt.get_time();                       // advance simulation time
            if( evt.get_type() == arrival ) ProcessArrival(evt);
            else  ProcessDeparture();
        }
        return ReportGeneration();
    }

    // Initialization method
    public void Initialization()   {
        Clock = 0.0;
        QueueLength = 0;
        NumberInService = 0;
        LastEventTime = 0.0;
        MaxQueueLength = 0;
        SumResponseTime = 0;
        NumberOfDepartures = 0;
        TotArrivals = 0;
        OccServersAcc = 0;
        CliQueueAcc = 0;
        CustomersInStationAcc = 0;
        SumDelayTime = -1;
        SumServiceTime = -1;

        serversBusyAcc = new double[Long.valueOf(NumberOfServers).intValue()];
        Arrays.fill(serversBusyAcc, 0);

        // create all the arrival events
        for(int i = 0; i < CurrCustomersInStation; i++) {
            double delayTime;
            // get the job at the head of the queue
            while (( delayTime = distDelays.extractValue(streamIndex * 2)) < 0 );
            if(SumDelayTime < 0) SumDelayTime = delayTime;
            else {
                SumDelayTime += delayTime;
            }
            Event evt = new Event(arrival, delayTime);
            FutureEventList.enqueue( evt );
        }
    }

    public void ProcessArrival(Event evt) {
        Customers.enqueue(evt);
        // Acc to calculate average queue length
        CliQueueAcc += QueueLength * (Clock - LastEventTime);
        // Acc to calculate the average number of clients in station
        CustomersInStationAcc += CurrCustomersInStation * (Clock - LastEventTime);
        QueueLength++;
        TotArrivals++;
        CurrCustomersInStation--;
        // Calculate the accumulator for the average number of Servers busy
        OccServersAcc += NumberInService * (Clock - LastEventTime);
        // Update the accumulators used to calculate the utilization of the single servers
        int i = 0;
        while (i < NumberInService) {
            serversBusyAcc[i] += (Clock - LastEventTime);
            i++;
        }
        // if the server is idle, fetch the event, do statistics
        // and put into service
        if(QueueLength - 1 == 0) {
            if(NumberInService < NumberOfServers) {
                ScheduleDeparture();
            }
        }

        // adjust max queue length statistics
        if (MaxQueueLength < QueueLength) MaxQueueLength = QueueLength;

        LastEventTime = Clock;
    }

    public void ScheduleDeparture() {
        double serviceTime;
        // get the job at the head of the queue
        while (( serviceTime = distServices.extractValue((streamIndex * 2) + 1)) < 0 );
        if(SumServiceTime < 0) SumServiceTime = serviceTime;
        else {
            SumServiceTime += serviceTime;
        }
        Event depart = new Event(departure,Clock + serviceTime);
        FutureEventList.enqueue( depart );
        NumberInService++;
        QueueLength--;
    }

    public void ProcessDeparture() {
        // get the customer description
        Event finished = (Event) Customers.dequeue();
        // Acc to calculate average queue length
        CliQueueAcc += QueueLength * (Clock - LastEventTime);
        // Acc to calculate the average number of clients in station
        CustomersInStationAcc += CurrCustomersInStation * (Clock - LastEventTime);
        // Calculate the accumulator for the average number of Servers busy
        OccServersAcc += NumberInService * (Clock - LastEventTime);
        // Update the accumulators used to calculate the utilization of the single servers
        int i = 0;
        while (i < NumberInService) {
            serversBusyAcc[i] += (Clock - LastEventTime);
            i++;
        }
        // if there are customers in the queue then schedule
        // the departure of the next one
        if(NumberInService > 0) NumberInService--;
        while(NumberInService < NumberOfServers && QueueLength > 0) {
            ScheduleDeparture();
        }

        // schedule the next arrival
        double delayTime;
        // get the job at the head of the queue
        while (( delayTime = distDelays.extractValue(streamIndex * 2)) < 0 );
        SumDelayTime += delayTime;
        Event next_arrival = new Event(arrival, Clock + delayTime);
        FutureEventList.enqueue( next_arrival );

        // measure the response time and add it to the sum
        double response = (Clock - finished.get_time());
        SumResponseTime += response;
        NumberOfDepartures++;
        CurrCustomersInStation++;
        LastEventTime = Clock;
    }

    public SimResultsM2 ReportGeneration() {
        // Calculate the average response time
        double AVGR  = SumResponseTime / NumberOfDepartures;
        // The meanInterArrivalTime mus be calculated using the total number of persons that entered the queue
        // If we consider just the number of customers served, when the average service time is bigger than the
        // average arrival time, there are people in queue that will never get served before the end of the simulation
        double meanDelayTime = SumDelayTime / TotArrivals;
        double meanServiceTime = SumServiceTime / NumberOfDepartures;

        // Calculate the average usage of the servers
        double totalUsage = 0;
        for(int i = 0; i < NumberOfServers; i++) {
            totalUsage += serversBusyAcc[i] / Clock;
        }
        totalUsage /= NumberOfServers;

        SimResultsM2 simRes = new SimResultsM2(meanDelayTime, meanServiceTime, serversBusyAcc, totalUsage,
                OccServersAcc / Clock, CliQueueAcc / Clock, MaxQueueLength, CustomersInStationAcc / Clock,(float) NumberOfDepartures / Clock,
                AVGR, Clock, TotCustomers);

        System.out.println( "\tMEAN DELAY TIME                                "
                + meanDelayTime );
        System.out.println( "\tMEAN SERVICE TIME                              "
                + meanServiceTime );
        System.out.println();
        // Calculate the utilization of the single servers
        for(int i = 0; i < NumberOfServers; i++) {
            serversBusyAcc[i] = serversBusyAcc[i] / Clock;
            System.out.println( "\tSERVER " + (i+1) + " UTILIZATION                           " + serversBusyAcc[i]);
        }
        System.out.println( "\tTOTAL UTILIZATION                              " + totalUsage );
        // Average number of servers busy
        System.out.println( "\tAVERAGE SERVERS BUSY                           " + simRes.avgServersBusy);
        System.out.println( "\tAVERAGE QUEUE LENGTH                           " + simRes.avgQueueLength);
        System.out.println( "\tAVERAGE CUSTOMERS IN STATION                   " + simRes.avgCliStation);
        System.out.println( "\tMAXIMUM QUEUE LENGTH                           " + MaxQueueLength );
        System.out.println( "\tTHROUGHPUT                                     " + simRes.throughput);
        System.out.println( "\tAVERAGE RESPONSE TIME                          " + AVGR + "  MINUTES" );
        System.out.println( "\tSIMULATION RUNLENGTH                           " + Clock + " MINUTES" );
        System.out.println( "\tNUMBER OF DEPARTURES                           " + NumberOfDepartures);

        return simRes;
    }
}