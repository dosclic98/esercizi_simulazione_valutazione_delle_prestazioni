package main;

import SimUtils.*;
import distributions.Distribution;

import java.util.Arrays;


public class Sim3 {

    // Class Sim variables
    public double Clock, SumDelayTime, SumServiceTime1, SumServiceTime2,  LastEventTime,
            SumResponseTime1, SumResponseTime2, OccServersAcc1, OccServersAcc2, p, accProbIneff;
    public long QueueLength1, QueueLength2, NumberInService1, NumberInService2, MaxQueueLength1, MaxQueueLength2, CustomersInStationAcc, TotCustomers,
            CurrCustomersInStation, NumberOfDepartures1, NumberOfDepartures2, TotArrivals, NumberOfServers, CliQueueAcc1, CliQueueAcc2, seed;

    // Integral accumulator for servers utilization
    public double[] serversBusyAcc1;
    public double[] serversBusyAcc2;

    public int streamIndex;

    // Event types
    public final int arrivalQ1 = 1;
    public final int arrivalQ2 = 2;
    public final int departureFromQ1 = 3;
    public final int departureFromQ2 = 4;

    public EventList FutureEventList;
    public Queue Customers1;
    public Queue Customers2;

    public Distribution distDelays;
    public Distribution distServices;

    public Rvgs rvgs;

    public Sim3(Distribution distDelay, Distribution distServ, int streamIndex,
                int customersInStation, int numServers, double  p, long seed) {
        CurrCustomersInStation = TotCustomers = customersInStation;
        distDelays = distDelay;
        distServices = distServ;
        NumberOfServers = numServers;
        this.streamIndex = streamIndex;
        this.p = p;
        this.seed = seed;
        Rngs rngs = new Rngs();
        rngs.plantSeeds(seed);
        rvgs = new Rvgs(rngs);
    }

    public SimResultsM3 runSim() {
        FutureEventList = new EventList();
        Customers1 = new Queue();
        Customers2 = new Queue();

        Initialization();

        // Loop until first "initialCustomersInStation * 500" customers have departed
        long initialCustomersInStation = CurrCustomersInStation;
        while((NumberOfDepartures1 + NumberOfDepartures2) < initialCustomersInStation * 500) {
            Event evt = FutureEventList.getMin();  // get imminent event
            FutureEventList.dequeue();                    // be rid of it
            Clock = evt.get_time();                       // advance simulation time
            if( evt.get_type() == arrivalQ1) ProcessArrivalQ1(evt);
            else if(evt.get_type() == arrivalQ2) ProcessArrivalQ2(evt);
            else if (evt.get_type() == departureFromQ1) ProcessDepartureQ1();
            else  ProcessDepartureQ2();
        }
        return ReportGeneration();
    }

    // Initialization method
    public void Initialization()   {
        Clock = 0.0;
        QueueLength1 = 0;
        QueueLength2 = 0;
        NumberInService1 = 0;
        NumberInService2 = 0;
        LastEventTime = 0.0;
        MaxQueueLength1 = 0;
        MaxQueueLength2 = 0;
        SumResponseTime1 = 0;
        SumResponseTime2 = 0;
        NumberOfDepartures1 = 0;
        NumberOfDepartures2 = 0;
        TotArrivals = 0;
        OccServersAcc1 = 0;
        OccServersAcc2 = 0;
        CliQueueAcc1 = 0;
        CliQueueAcc2 = 0;
        CustomersInStationAcc = 0;
        SumDelayTime = -1;
        SumServiceTime1 = -1;
        SumServiceTime2 = -1;
        accProbIneff = 0;

        serversBusyAcc1 = new double[Long.valueOf(NumberOfServers).intValue()];
        serversBusyAcc2 = new double[Long.valueOf(NumberOfServers).intValue()];
        Arrays.fill(serversBusyAcc1, 0);
        Arrays.fill(serversBusyAcc2, 0);

        // create all the arrival events
        for(int i = 0; i < CurrCustomersInStation; i++) {
            double delayTime;
            // get the job at the head of the queue
            while (( delayTime = distDelays.extractValue(streamIndex * 2)) < 0 );
            if(SumDelayTime < 0) SumDelayTime = delayTime;
            else {
                SumDelayTime += delayTime;
            }
            int arrival = extractArrivalType();
            Event evt = new Event(arrival, delayTime);
            FutureEventList.enqueue( evt );
        }
    }

    public int extractArrivalType() {
        double value = rvgs.uniform(0,1);
        if(value <= p) return arrivalQ1;
        else return arrivalQ2;
    }

    public void ProcessArrivalQ1(Event evt) {
        Customers1.enqueue(evt);
        // Update the accumulator to calculate the probability of inefficiency
        if((NumberInService2 == 0 && QueueLength2 == 0 && QueueLength1 >= 1) ||
                (NumberInService1 == 0 && QueueLength1 == 0 && QueueLength2 >= 1)) {
            accProbIneff += (Clock - LastEventTime);
        }
        // Acc to calculate average queue length
        CliQueueAcc1 += QueueLength1 * (Clock - LastEventTime);
        CliQueueAcc2 += QueueLength2 * (Clock - LastEventTime);
        // Acc to calculate the average number of clients in station
        CustomersInStationAcc += CurrCustomersInStation * (Clock - LastEventTime);
        QueueLength1++;
        TotArrivals++;
        CurrCustomersInStation--;
        // Calculate the accumulator for the average number of Servers busy
        OccServersAcc1 += NumberInService1 * (Clock - LastEventTime);
        OccServersAcc2 += NumberInService2 * (Clock - LastEventTime);
        // Update the accumulators used to calculate the utilization of the single servers
        int i = 0;
        while (i < NumberInService1) {
            serversBusyAcc1[i] += (Clock - LastEventTime);
            i++;
        }
        i = 0;
        while (i < NumberInService2) {
            serversBusyAcc2[i] += (Clock - LastEventTime);
            i++;
        }

        // if the server is idle, fetch the event, do statistics
        // and put into service
        if(QueueLength1 - 1 == 0) {
            if(NumberInService1 < NumberOfServers) {
                ScheduleDepartureQ1();
            }
        }

        // adjust max queue length statistics
        if (MaxQueueLength1 < QueueLength1) MaxQueueLength1 = QueueLength1;

        LastEventTime = Clock;
    }

    public void ProcessArrivalQ2(Event evt) {
        Customers2.enqueue(evt);
        // Update the accumulator to calculate the probability of inefficiency
        if((NumberInService2 == 0 && QueueLength2 == 0 && QueueLength1 >= 1) ||
            (NumberInService1 == 0 && QueueLength1 == 0 && QueueLength2 >= 1)) {
            accProbIneff += (Clock - LastEventTime);
        }
        // Acc to calculate average queue length
        CliQueueAcc1 += QueueLength1 * (Clock - LastEventTime);
        CliQueueAcc2 += QueueLength2 * (Clock - LastEventTime);
        // Acc to calculate the average number of clients in station
        CustomersInStationAcc += CurrCustomersInStation * (Clock - LastEventTime);
        QueueLength2++;
        TotArrivals++;
        CurrCustomersInStation--;
        // Calculate the accumulator for the average number of Servers busy
        OccServersAcc1 += NumberInService1 * (Clock - LastEventTime);
        OccServersAcc2 += NumberInService2 * (Clock - LastEventTime);
        // Update the accumulators used to calculate the utilization of the single servers
        int i = 0;
        while (i < NumberInService1) {
            serversBusyAcc1[i] += (Clock - LastEventTime);
            i++;
        }
        i = 0;
        while (i < NumberInService2) {
            serversBusyAcc2[i] += (Clock - LastEventTime);
            i++;
        }

        // if the server is idle, fetch the event, do statistics
        // and put into service
        if(QueueLength2 - 1 == 0) {
            if(NumberInService2 < NumberOfServers) {
                ScheduleDepartureQ2();
            }
        }

        // adjust max queue length statistics
        if (MaxQueueLength2 < QueueLength2) MaxQueueLength2 = QueueLength2;

        LastEventTime = Clock;
    }

    public void ScheduleDepartureQ1() {
        double serviceTime;
        // get the job at the head of the queue
        while (( serviceTime = distServices.extractValue((streamIndex * 2) + 1)) < 0 );
        if(SumServiceTime1 < 0) SumServiceTime1 = serviceTime;
        else {
            SumServiceTime1 += serviceTime;
        }
        Event depart = new Event(departureFromQ1,Clock + serviceTime);
        FutureEventList.enqueue( depart );
        NumberInService1++;
        QueueLength1--;
    }

    public void ScheduleDepartureQ2() {
        double serviceTime;
        // get the job at the head of the queue
        while (( serviceTime = distServices.extractValue((streamIndex * 2) + 1)) < 0 );
        if(SumServiceTime2 < 0) SumServiceTime2 = serviceTime;
        else {
            SumServiceTime2 += serviceTime;
        }
        Event depart = new Event(departureFromQ2,Clock + serviceTime);
        FutureEventList.enqueue( depart );
        NumberInService2++;
        QueueLength2--;
    }

    public void ProcessDepartureQ1() {
        // get the customer description
        Event finished = (Event) Customers1.dequeue();
        // Update the accumulator to calculate the probability of inefficiency
        if((NumberInService2 == 0 && QueueLength2 == 0 && QueueLength1 >= 1) ||
                (NumberInService1 == 0 && QueueLength1 == 0 && QueueLength2 >= 1)) {
            accProbIneff += (Clock - LastEventTime);
        }
        // Acc to calculate average queue length
        CliQueueAcc1 += QueueLength1 * (Clock - LastEventTime);
        CliQueueAcc2 += QueueLength2 * (Clock - LastEventTime);
        // Acc to calculate the average number of clients in station
        CustomersInStationAcc += CurrCustomersInStation * (Clock - LastEventTime);
        // Calculate the accumulator for the average number of Servers busy
        OccServersAcc1 += NumberInService1 * (Clock - LastEventTime);
        OccServersAcc2 += NumberInService2 * (Clock - LastEventTime);
        int i = 0;
        while (i < NumberInService1) {
            serversBusyAcc1[i] += (Clock - LastEventTime);
            i++;
        }
        i = 0;
        while (i < NumberInService2) {
            serversBusyAcc2[i] += (Clock - LastEventTime);
            i++;
        }
        // if there are customers in the queue then schedule
        // the departure of the next one
        if(NumberInService1 > 0) NumberInService1--;
        while(NumberInService1 < NumberOfServers && QueueLength1 > 0) {
            ScheduleDepartureQ1();
        }


        // schedule the next arrival
        double delayTime;
        // get the job at the head of the queue
        while (( delayTime = distDelays.extractValue(streamIndex * 2)) < 0 );
        SumDelayTime += delayTime;
        int arrival = extractArrivalType();
        Event next_arrival = new Event(arrival, Clock + delayTime);
        FutureEventList.enqueue( next_arrival );

        // measure the response time and add it to the sum
        double response = (Clock - finished.get_time());
        SumResponseTime1 += response;
        NumberOfDepartures1++;
        CurrCustomersInStation++;
        LastEventTime = Clock;
    }

    public void ProcessDepartureQ2() {
        // get the customer description
        Event finished = (Event) Customers2.dequeue();
        // Update the accumulator to calculate the probability of inefficiency
        if((NumberInService2 == 0 && QueueLength2 == 0 && QueueLength1 >= 1) ||
                (NumberInService1 == 0 && QueueLength1 == 0 && QueueLength2 >= 1)) {
            accProbIneff += (Clock - LastEventTime);
        }
        // Acc to calculate average queue length
        CliQueueAcc1 += QueueLength1 * (Clock - LastEventTime);
        CliQueueAcc2 += QueueLength2 * (Clock - LastEventTime);
        // Acc to calculate the average number of clients in station
        CustomersInStationAcc += CurrCustomersInStation * (Clock - LastEventTime);
        // Calculate the accumulator for the average number of Servers busy
        OccServersAcc1 += NumberInService1 * (Clock - LastEventTime);
        OccServersAcc2 += NumberInService2 * (Clock - LastEventTime);
        // Update the accumulators used to calculate the utilization of the single servers
        int i = 0;
        while (i < NumberInService1) {
            serversBusyAcc1[i] += (Clock - LastEventTime);
            i++;
        }
        i = 0;
        while (i < NumberInService2) {
            serversBusyAcc2[i] += (Clock - LastEventTime);
            i++;
        }
        // if there are customers in the queue then schedule
        // the departure of the next one
        if(NumberInService2 > 0) NumberInService2--;
        while(NumberInService2 < NumberOfServers && QueueLength2 > 0) {
            ScheduleDepartureQ2();
        }


        // schedule the next arrival
        double delayTime;
        // get the job at the head of the queue
        while (( delayTime = distDelays.extractValue(streamIndex * 2)) < 0 );
        SumDelayTime += delayTime;
        int arrival = extractArrivalType();
        Event next_arrival = new Event(arrival, Clock + delayTime);
        FutureEventList.enqueue( next_arrival );

        // measure the response time and add it to the sum
        double response = (Clock - finished.get_time());
        SumResponseTime2 += response;
        NumberOfDepartures2++;
        CurrCustomersInStation++;
        LastEventTime = Clock;
    }


    public SimResultsM3 ReportGeneration() {
        // Calculate the average response time
        double AVGR1  = SumResponseTime1 / NumberOfDepartures1;
        double AVGR2  = SumResponseTime2 / NumberOfDepartures2;
        // The meanInterArrivalTime mus be calculaed using the total number of persons that enered the queue
        // If we consider just the number of cutomers served, when the average service time is bigger than the
        // average arrival time, there are people in queue that will never get served before the end of the simulation
        double meanDelayTime = SumDelayTime / TotArrivals;
        double meanServiceTime1 = SumServiceTime1 / NumberOfDepartures1;
        double meanServiceTime2 = SumServiceTime2 / NumberOfDepartures2;

        // Calculate the average usage of the servers for queue 1 and queue 2
        double totalUsage1 = 0;
        for(int i = 0; i < NumberOfServers; i++) {
            totalUsage1 += serversBusyAcc1[i] / Clock;
        }
        totalUsage1 /= NumberOfServers;

        double totalUsage2 = 0;
        for(int i = 0; i < NumberOfServers; i++) {
            totalUsage2 += serversBusyAcc2[i] / Clock;
        }
        totalUsage2 /= NumberOfServers;

        SimResultsM3 simRes = new SimResultsM3(meanDelayTime, meanServiceTime1, meanServiceTime2, serversBusyAcc1, serversBusyAcc2, totalUsage1, totalUsage2,
                OccServersAcc1 / Clock,OccServersAcc2 / Clock, CliQueueAcc1 / Clock, CliQueueAcc2 / Clock,
                MaxQueueLength1, MaxQueueLength2, CustomersInStationAcc / Clock,NumberOfDepartures1 / Clock, NumberOfDepartures2 / Clock,
                AVGR1, AVGR2, Clock, TotCustomers, accProbIneff / Clock);

        System.out.println( "\tMEAN DELAY TIME                                "
                + meanDelayTime );
        System.out.println( "\tMEAN SERVICE TIME 1                            "
                + meanServiceTime1 );
        System.out.println( "\tMEAN SERVICE TIME 2                            "
                + meanServiceTime2 );
        System.out.println();
        // Calculate the utilization of the single servers for queue 1 and queue 2
        for(int i = 0; i < NumberOfServers; i++) {
            serversBusyAcc1[i] = serversBusyAcc1[i] / Clock;
            System.out.println( "\tQUEUE 1 SERVER " + (i+1) + " UTILIZATION                   " + serversBusyAcc1[i]);
        }
        for(int i = 0; i < NumberOfServers; i++) {
            serversBusyAcc2[i] = serversBusyAcc2[i] / Clock;
            System.out.println( "\tQUEUE 2 SERVER " + (i+1) + " UTILIZATION                   " + serversBusyAcc2[i]);
        }
        System.out.println( "\tTOTAL UTILIZATION 1                            " + totalUsage1 );
        System.out.println( "\tTOTAL UTILIZATION 2                            " + totalUsage2 );
        // Average number of servers busy
        System.out.println( "\tAVERAGE SERVERS BUSY 1                         " + simRes.avgServersBusy1);
        System.out.println( "\tAVERAGE SERVERS BUSY 2                         " + simRes.avgServersBusy2);
        System.out.println( "\tAVERAGE QUEUE LENGTH 1                         " + simRes.avgQueueLength1);
        System.out.println( "\tAVERAGE QUEUE LENGTH 2                         " + simRes.avgQueueLength2);
        System.out.println( "\tAVERAGE CUSTOMERS IN STATION                   " + simRes.avgCliStation);
        System.out.println( "\tMAXIMUM QUEUE LENGTH 1                         " + MaxQueueLength1);
        System.out.println( "\tMAXIMUM QUEUE LENGTH 2                         " + MaxQueueLength2);
        System.out.println( "\tTHROUGHPUT  1                                  " + simRes.throughput1);
        System.out.println( "\tTHROUGHPUT  2                                  " + simRes.throughput2);
        System.out.println( "\tAVERAGE RESPONSE TIME 1                        " + AVGR1 + "  MINUTES" );
        System.out.println( "\tAVERAGE RESPONSE TIME 2                        " + AVGR2 + "  MINUTES" );
        System.out.println( "\tSIMULATION RUNLENGTH                           " + Clock + " MINUTES" );
        System.out.println( "\tNUMBER OF DEPARTURES                           " + (NumberOfDepartures1 + NumberOfDepartures2));
        System.out.println( "\tINEFFICIENCY PROBABILITY                       " + simRes.probIneff);

        return simRes;
    }
}