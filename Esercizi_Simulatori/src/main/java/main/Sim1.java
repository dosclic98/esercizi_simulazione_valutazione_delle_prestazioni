package main;

import SimUtils.Event;
import SimUtils.EventList;
import SimUtils.Queue;
import SimUtils.SimResultsM1;
import distributions.Distribution;

import java.util.Arrays;


public class Sim1 {

    // Class Sim variables
    public double Clock, SumInterArrivalTime, SumServiceTime, LastEventTime,
            SumResponseTime, OccServersAcc;
    public long QueueLength, NumberInService, MaxQueueLength,
            TotalCustomers, NumberOfDepartures, TotArrivals, NumberOfServers, CliQueueAcc;

    // Integral accumulator for servers utilization
    public double[] serversBusyAcc;

    public int streamIndex;

    // Event types
    public final int arrival = 1;
    public final int departure = 2;

    public EventList FutureEventList;
    public Queue Customers;

    public Distribution distArrivals;
    public Distribution distServices;

    public Sim1(Distribution distArr, Distribution distServ, int streamIndex, int totalCustomers, int numServers) {
        TotalCustomers = totalCustomers;
        distArrivals = distArr;
        distServices = distServ;
        NumberOfServers = numServers;
        this.streamIndex = streamIndex;
    }

    public SimResultsM1 runSim() {
        FutureEventList = new EventList();
        Customers = new Queue();

        Initialization();

        // Loop until first "TotalCustomers" have departed
        while(NumberOfDepartures < TotalCustomers) {
            Event evt = FutureEventList.getMin();  // get imminent event
            FutureEventList.dequeue();                    // be rid of it
            Clock = evt.get_time();                       // advance simulation time
            if( evt.get_type() == arrival ) ProcessArrival(evt);
            else  ProcessDeparture();
        }
        return ReportGeneration();
    }

    // Initialization method
    public void Initialization()   {
        Clock = 0.0;
        QueueLength = 0;
        NumberInService = 0;
        LastEventTime = 0.0;
        MaxQueueLength = 0;
        SumResponseTime = 0;
        NumberOfDepartures = 0;
        TotArrivals = 0;
        OccServersAcc = 0;
        CliQueueAcc = 0;
        SumInterArrivalTime = -1;
        SumServiceTime = -1;

        serversBusyAcc = new double[Long.valueOf(NumberOfServers).intValue()];
        Arrays.fill(serversBusyAcc, 0);

        // Create first arrival event
        double arrivalTime;
        while (( arrivalTime = distArrivals.extractValue(streamIndex * 2)) < 0 );
        Event evt = new Event(arrival, arrivalTime);
        SumInterArrivalTime = arrivalTime;
        FutureEventList.enqueue( evt );
    }

    public void ProcessArrival(Event evt) {
        // Enqueue the customer
        Customers.enqueue(evt);
        // Acc to calculate average queue length
        CliQueueAcc += QueueLength * (Clock - LastEventTime);
        QueueLength++;
        // Calculate the accumulator for the average number of servers busy
        OccServersAcc += NumberInService * (Clock - LastEventTime);
        // Update the accumulators used to calculate the utilization of the single servers
        int i = 0;
        while (i < NumberInService) {
            serversBusyAcc[i] += (Clock - LastEventTime);
            i++;
        }
        // if the server is idle, fetch the event, do statistics
        // and put into service
        if(QueueLength - 1 == 0) {
            if(NumberInService < NumberOfServers) {
                ScheduleDeparture();
            }
        }

        // adjust max queue length statistics
        if (MaxQueueLength < QueueLength) MaxQueueLength = QueueLength;

        // schedule the next arrival
        double arrivalTime;
        // get the job at the head of the queue
        while (( arrivalTime = distArrivals.extractValue(streamIndex * 2)) < 0 );
        SumInterArrivalTime += arrivalTime;
        TotArrivals++;
        Event next_arrival = new Event(arrival, Clock + arrivalTime);
        FutureEventList.enqueue( next_arrival );
        LastEventTime = Clock;
    }

    public void ScheduleDeparture() {
        double serviceTime;
        // get the job at the head of the queue
        while (( serviceTime = distServices.extractValue((streamIndex * 2) + 1)) < 0 );
        if(SumServiceTime < 0) SumServiceTime = serviceTime;
        else {
            SumServiceTime += serviceTime;
        }
        Event depart = new Event(departure,Clock + serviceTime);
        FutureEventList.enqueue( depart );
        NumberInService++;
        QueueLength--;
    }

    public void ProcessDeparture() {
        // get the customer description
        Event finished = (Event) Customers.dequeue();
        // Acc to calculate average queue length
        CliQueueAcc += QueueLength * (Clock - LastEventTime);
        // Calculate the accumulator for the average number of Servers busy
        OccServersAcc += NumberInService * (Clock - LastEventTime);
        // Update the accumulators used to calculate the utilization of the single servers
        int i = 0;
        while (i < NumberInService) {
            serversBusyAcc[i] += (Clock - LastEventTime);
            i++;
        }
        // if there are customers in the queue then schedule
        // the departure of the next one
        if(NumberInService > 0) NumberInService--;
        while(NumberInService < NumberOfServers && QueueLength > 0) {
            ScheduleDeparture();
        }

        // measure the response time and add it to the sum
        double response = (Clock - finished.get_time());
        SumResponseTime += response;
        NumberOfDepartures++;
        LastEventTime = Clock;
    }

    public SimResultsM1 ReportGeneration() {
        // Calculate the average response time
        double AVGR  = SumResponseTime/TotalCustomers;
        // The meanInterArrivalTime mus be calculated using the total number of persons that entered the queue
        // If we consider just the number of customers served, when the average service time is bigger than the
        // average arrival time, there are people in queue that will never get served before the end of the simulation
        double meanInterArrivalTime = SumInterArrivalTime / TotArrivals;
        double meanServiceTime = SumServiceTime / TotalCustomers;

        // Calculate the average usage of the servers
        double totalUsage = 0;
        for(int i = 0; i < NumberOfServers; i++) {
            totalUsage += serversBusyAcc[i] / Clock;
        }
        totalUsage /= NumberOfServers;

        SimResultsM1 simRes = new SimResultsM1(meanInterArrivalTime, meanServiceTime, serversBusyAcc, totalUsage,
                OccServersAcc / Clock, CliQueueAcc / Clock, MaxQueueLength, (float)TotalCustomers / Clock,
                AVGR, Clock, TotalCustomers);

        System.out.println( "\tMEAN INTERARRIVAL TIME                         "
                + meanInterArrivalTime );
        System.out.println( "\tMEAN SERVICE TIME                              "
                + meanServiceTime );
        System.out.println();
        // Calculate the utilization of the single servers
        for(int i = 0; i < NumberOfServers; i++) {
            serversBusyAcc[i] = serversBusyAcc[i] / Clock;
            System.out.println( "\tSERVER " + (i+1) + " UTILIZATION                           " + serversBusyAcc[i]);
        }
        System.out.println( "\tTOTAL UTILIZATION                              " + totalUsage );
        // Average number of servers busy
        System.out.println( "\tAVERAGE SERVERS BUSY                           " + simRes.avgServersBusy);
        System.out.println( "\tAVERAGE QUEUE LENGTH                           " + simRes.avgQueueLength);
        System.out.println( "\tMAXIMUM QUEUE LENGTH                           " + MaxQueueLength );
        System.out.println( "\tTHROUGHPUT                                     " + simRes.throughput);
        System.out.println( "\tAVERAGE RESPONSE TIME                          " + AVGR + "  MINUTES" );
        System.out.println( "\tSIMULATION RUNLENGTH                           " + Clock + " MINUTES" );
        System.out.println( "\tNUMBER OF DEPARTURES                           " + TotalCustomers );

        return simRes;
    }
}