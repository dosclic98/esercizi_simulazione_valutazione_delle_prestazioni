package distributions;

import SimUtils.Rvgs;

public class Erlang extends Distribution {
    int k;
    double u;

    public static Erlang createErlang(Rvgs rvg, int k, double u) {
        if(k < 1) {
            System.err.println("The parameter k must be greater or equal to 1");
            return null;
        }
        if(u < 0) {
            System.err.println("A positive mean must be defined (we are using positive times)");
            return null;
        }

        return new Erlang(rvg, k, u);
    }

    private Erlang(Rvgs rvg, int k, double u) {
        this.rvg = rvg;
        this.k = k;
        this.u = u;
    }

    @Override
    public synchronized double extractValue(int streamIndex) {
        rvg.selectStream(streamIndex);
        return rvg.erlang(k, u);
    }
}
