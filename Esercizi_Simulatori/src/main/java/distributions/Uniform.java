package distributions;

import SimUtils.Rvgs;

public class Uniform extends Distribution {
    private final double min;
    private final double max;

    public static Uniform createUniform(Rvgs rvg, double min, double max) {
        if(min > max) {
            System.err.println("Max must be greater than min");
            return null;
        }
        if(min < 0) {
            System.err.println("Min must be positive");
            return null;
        }
        if(max < 0) {
            System.err.println("Max must be positive");
            return null;
        }

        return new Uniform(rvg, min, max);
    }

    private Uniform(Rvgs rvg, double min, double max) {
        this.rvg = rvg;
        this.min = min;
        this.max = max;
    }

    // Returns a number between max (inclusive) and min (exclusive)
    @Override
    public synchronized double extractValue(int streamIndex) {
        rvg.selectStream(streamIndex);
        return rvg.uniform(min, max);
    }
}
