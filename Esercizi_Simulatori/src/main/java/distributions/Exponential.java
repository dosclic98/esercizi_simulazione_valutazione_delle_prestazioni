package distributions;

import SimUtils.Rvgs;

public class Exponential extends Distribution {
    double mean;

    public static Exponential createExponential(Rvgs rvg, double mean) {
        if(mean < 0) {
            System.err.println("A positive mean must be defined (we are using positive times)");
            return null;
        }
        return new Exponential(rvg, mean);
    }

    private Exponential(Rvgs rvg, double mean) {
        this.rvg = rvg;
        this.mean = mean;
    }


    @Override
    public synchronized double extractValue(int streamIndex) {
        rvg.selectStream(streamIndex);
        return rvg.exponential(mean);
    }
}
