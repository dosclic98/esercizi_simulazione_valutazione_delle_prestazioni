package distributions;

import SimUtils.Rvgs;

public abstract class Distribution {
    protected Rvgs rvg;
    public abstract double extractValue(int streamIndex);
}
