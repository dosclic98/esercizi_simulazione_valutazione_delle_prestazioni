package distributions;

import SimUtils.Rvgs;

public class Normal extends Distribution {
    double mean;
    double sigma;

    public static Normal createNormal(Rvgs rvg, double mean, double sigma) {
        if(mean < 0) {
            System.err.println("A positive mean must be defined (we are using positive times)");
            return null;
        }
        if(sigma < 0) {
            System.err.println("The standard deviation og the normal distribution must be positive");
            return null;
        }

        return new Normal(rvg, mean, sigma);
    }

    private Normal(Rvgs rvg, double mean, double sigma) {
        this.rvg = rvg;
        this.mean = mean;
        this.sigma = sigma;
    }

    @Override
    public synchronized double extractValue(int streamIndex) {
        rvg.selectStream(streamIndex);
        return rvg.normal(mean, sigma);
    }
}
