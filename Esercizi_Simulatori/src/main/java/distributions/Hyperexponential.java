package distributions;

import SimUtils.Rvgs;

public class Hyperexponential extends Distribution{
    double p;
    Exponential exp1;
    Exponential exp2;

    public static Hyperexponential createHyperexponential(Rvgs rvg, double p, double mean1, double mean2) {
        Exponential tmpExp1 = Exponential.createExponential(rvg, mean1);
        Exponential tmpExp2 = Exponential.createExponential(rvg, mean2);
        if(tmpExp1 == null || tmpExp2 == null) {
            System.err.println("Error creating the Hyperexponential");
            return null;
        }
        if(p < 0 || p > 1) {
            System.err.println("The probability of the hyperexponential must be a number between 0 and 1");
            return null;
        }
        return new Hyperexponential(rvg, p, tmpExp1, tmpExp2);
    }

    private Hyperexponential(Rvgs rvg, double p, Exponential exp1, Exponential exp2) {
        this.rvg = rvg;
        this.p = p;
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    @Override
    public synchronized double extractValue(int streamIndex) {
        rvg.selectStream(streamIndex);
        double gen = rvg.uniform(0, 1);
        if(gen <= p) {
            return exp1.extractValue(streamIndex);
        } else {
            return exp2.extractValue(streamIndex);
        }
    }
}
