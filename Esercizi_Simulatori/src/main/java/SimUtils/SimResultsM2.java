package SimUtils;

public class SimResultsM2{
    public double meanDelayTime;
    public double meanServiceTime;
    public double[] serverBusy;
    public double totalBusy;
    public double avgServersBusy;
    public double avgQueueLength;
    public long maxQueueLength;
    public double avgCliStation;
    public double throughput;
    public double avgResTime;
    public double simTotTime;
    public long totalCustomers;

    public SimResultsM2(
            double meanDelayTime,
            double meanServiceTime,
            double[] serverBusy,
            double totalBusy,
            double avgServersBusy,
            double avgQueueLength,
            long maxQueueLength,
            double avgCliStation,
            double throughput,
            double avgResTime,
            double simTotTime,
            long totalCustomers) {
        this.meanDelayTime = meanDelayTime;
        this.meanServiceTime = meanServiceTime;
        this.serverBusy = serverBusy;
        this.totalBusy = totalBusy;
        this.avgServersBusy = avgServersBusy;
        this.avgQueueLength = avgQueueLength;
        this.maxQueueLength = maxQueueLength;
        this.avgCliStation = avgCliStation;
        this.throughput = throughput;
        this.avgResTime = avgResTime;
        this.simTotTime = simTotTime;
        this.totalCustomers = totalCustomers;
    }

}
