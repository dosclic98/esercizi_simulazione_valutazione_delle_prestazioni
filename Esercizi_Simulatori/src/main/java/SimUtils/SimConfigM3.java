package SimUtils;

import distributions.Distribution;

public class SimConfigM3 extends SimConfigM1M2{
    public Double p;
    public long seed;

    public SimConfigM3(Distribution arrivalDist, Distribution serviceDist,
                       Long customers, Long servers, Long runs, Double confidence, Double p, Long seed) {
        super(arrivalDist, serviceDist, customers, servers, runs, confidence);
        this.p = p;
        this.seed = seed;
    }

    public SimConfigM3(SimConfigM1M2 config, Double p, Long seed) {
        this(config.arrivalDist, config.serviceDist, config.customers, config.servers,
                config.runs, config.confidence, p, seed);
    }
}
