 package SimUtils;

import java.util.ArrayList;

 public class SimOutAnalysisM3 extends SimOutAnalysisTemplate{
     private final ArrayList<SimResultsM3> simResults;

     public SimOutAnalysisM3(ArrayList<SimResultsM3> simResults, Double confidence, Long numRuns) {
         this.numRuns = numRuns;
         this.simResults = simResults;
         this.confidInterval = confidence;
     }

     public void doAnalysis() {
         ArrayList<Double> meanInterArrivalTimes = new ArrayList<>();
         ArrayList<Double> meanServiceTimes1 = new ArrayList<>();
         ArrayList<Double> meanServiceTimes2 = new ArrayList<>();
         ArrayList<Double> totalBusyList1 = new ArrayList<>();
         ArrayList<Double> totalBusyList2 = new ArrayList<>();
         ArrayList<Double> avgServerBusyList1 = new ArrayList<>();
         ArrayList<Double> avgServerBusyList2 = new ArrayList<>();
         ArrayList<Double> avgQueueLengthList1 = new ArrayList<>();
         ArrayList<Double> avgQueueLengthList2 = new ArrayList<>();
         ArrayList<Long> maxQueueLengthList1 = new ArrayList<>();
         ArrayList<Long> maxQueueLengthList2 = new ArrayList<>();
         ArrayList<Double> avgCliStationList = new ArrayList<>();
         ArrayList<Double> throughputList1 = new ArrayList<>();
         ArrayList<Double> throughputList2 = new ArrayList<>();
         ArrayList<Double> avgResTimeList1 = new ArrayList<>();
         ArrayList<Double> avgResTimeList2 = new ArrayList<>();
         ArrayList<Double> simTotTimeList = new ArrayList<>();
         ArrayList<Double> probIneffList = new ArrayList<>();

         ArrayList<ArrayList<Double>> serverBusyList1 = new ArrayList<>();
         for(int j = 0; j < simResults.get(0).serverBusy1.length; j++) {
             serverBusyList1.add(new ArrayList<>());
         }
         ArrayList<ArrayList<Double>> serverBusyList2 = new ArrayList<>();
         for(int j = 0; j < simResults.get(0).serverBusy2.length; j++) {
             serverBusyList2.add(new ArrayList<>());
         }

         for(SimResultsM3 res : simResults) {
             for(int i = 0; i < res.serverBusy1.length; i++) {
                 serverBusyList1.get(i).add(res.serverBusy1[i]);
             }
             for(int i = 0; i < res.serverBusy2.length; i++) {
                 serverBusyList2.get(i).add(res.serverBusy2[i]);
             }
             meanInterArrivalTimes.add(res.meanDelayTime);
             meanServiceTimes1.add(res.meanServiceTime1);
             meanServiceTimes2.add(res.meanServiceTime2);
             totalBusyList1.add(res.totalBusy1);
             totalBusyList2.add(res.totalBusy2);
             avgServerBusyList1.add(res.avgServersBusy1);
             avgServerBusyList2.add(res.avgServersBusy2);
             avgQueueLengthList1.add(res.avgQueueLength1);
             avgQueueLengthList2.add(res.avgQueueLength2);
             maxQueueLengthList1.add(res.maxQueueLength1);
             maxQueueLengthList2.add(res.maxQueueLength2);
             avgCliStationList.add(res.avgCliStation);
             throughputList1.add(res.throughput1);
             throughputList2.add(res.throughput2);
             avgResTimeList1.add(res.avgResTime1);
             avgResTimeList2.add(res.avgResTime2);
             simTotTimeList.add(res.simTotTime);
             probIneffList.add(res.probIneff);
         }
         System.out.println();
         doubleEstimation(meanInterArrivalTimes, "#### Mean Delay times ####");
         doubleEstimation(meanServiceTimes1, "#### Mean Service times Queue 1 ####");
         doubleEstimation(meanServiceTimes2, "#### Mean Service times Queue 2 ####");
         for(int k = 0; k < serverBusyList1.size(); k++) {
             ArrayList<Double> serverBusy = serverBusyList1.get(k);
             doubleEstimation(serverBusy, "#### Queue 1 Server " + (k+1) + " utilization ####");
         }
         for(int k = 0; k < serverBusyList2.size(); k++) {
             ArrayList<Double> serverBusy = serverBusyList2.get(k);
             doubleEstimation(serverBusy, "#### Queue 2 Server " + (k+1) + " utilization ####");
         }
         doubleEstimation(totalBusyList1, "#### Total busy 1 ####");
         doubleEstimation(totalBusyList2, "#### Total busy 2 ####");
         doubleEstimation(avgServerBusyList1, "#### Average servers busy 1 ####");
         doubleEstimation(avgServerBusyList2, "#### Average servers busy 2 ####");
         doubleEstimation(avgQueueLengthList1, "#### Average queue length 1 ####");
         doubleEstimation(avgQueueLengthList2, "#### Average queue length 2 ####");
         longEstimation(maxQueueLengthList1, "#### Max queue length 1 ####");
         longEstimation(maxQueueLengthList2, "#### Max queue length 2 ####");
         doubleEstimation(throughputList1, "#### Throughput 1 ####");
         doubleEstimation(throughputList2, "#### Throughput 2 ####");
         doubleEstimation(avgCliStationList, "#### Average clients in station ####");
         doubleEstimation(avgResTimeList1, "#### Average response time 1 ####");
         doubleEstimation(avgResTimeList2, "#### Average response time 2 ####");
         doubleEstimation(simTotTimeList, "#### Sim total time ####");
         doubleEstimation(probIneffList, "#### Probability of inefficiency ####");
     }
 }
