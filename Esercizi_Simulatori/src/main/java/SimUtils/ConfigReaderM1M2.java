package SimUtils;

import distributions.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

import static distributions.Uniform.createUniform;

public class ConfigReaderM1M2 {
    protected static final String configFilePath = "config/";
    private static final String arrivalDistKey = "arrivalDist";
    private static final String serviceDistKey = "serviceDist";
    private static final String seedKey = "seed";
    private static final String customersKey = "customers";
    private static final String serversKey = "servers";
    private static final String runsKey = "runs";
    private static final String confidenceKey = "confidence";

    private static final String expoDistKey = "expo";
    private static final String uniformDistKey = "uniform";
    private static final String normalDistKey = "normal";
    private static final String hyperDistKey = "hyper";
    private static final String erlangDistKey = "erlang";

    private static final String meanKey = "mean";
    private static final String kKey = "k";
    private static final String pKey = "p";
    private static final String mean1Key = "mean1";
    private static final String mean2Key = "mean2";
    private static final String sigmaKey = "sigma";
    private static final String minKey = "min";
    private static final String maxKey = "max";

    String fileName;
    Long seed = null;

    public ConfigReaderM1M2(String fileName) {
        this.fileName = fileName;
    }

    public SimConfigM1M2 readConfig() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(configFilePath + fileName));
        JSONObject arrivalDistJson = (JSONObject) jsonObject.get(arrivalDistKey);
        JSONObject serviceDistJson = (JSONObject) jsonObject.get(serviceDistKey);
        seed = (Long) jsonObject.get(seedKey);
        Long customers = (Long) jsonObject.get(customersKey);
        Long servers = (Long) jsonObject.get(serversKey);
        Long runs = (Long) jsonObject.get(runsKey);
        Double confidence = (Double) jsonObject.get(confidenceKey);
        if(arrivalDistJson == null || serviceDistJson == null || seed == null || servers == null || customers == null || runs == null || confidence == null) {
            throw new IllegalStateException("The configuration file must contain an arrival time distribution, a service time distribution, a seed, the number of customers, the number of servers, the number of runs and the level of confidence to use in the interval analysis");
        }
        if(servers < 1) {
            throw new IllegalArgumentException("The number of servers must be at least 1");
        }
        if(customers < 1) {
            throw new IllegalArgumentException("The number of customers must be at least 1");
        }
        if(runs < 1) {
            throw new IllegalArgumentException("The number of runs must be at least 1");
        }
        if(confidence < 1 || confidence > 100) {
            throw new IllegalArgumentException("The level of confidence must be between 1 and 100");
        }
        Rngs rng = new Rngs();
        rng.plantSeeds(seed);
        Rvgs rvg = new Rvgs(rng);

        Distribution arrivalDist = parseDistribution(arrivalDistJson, rvg);
        Distribution serviceDist = parseDistribution(serviceDistJson, rvg);

        if(arrivalDist == null) {
            throw new IllegalArgumentException("You have to specify an arrival time distribution with the correct format");
        }
        if(serviceDist == null) {
            throw new IllegalArgumentException("You have to specify a service time distribution with the correct format");
        }

        return new SimConfigM1M2(arrivalDist, serviceDist, customers, servers, runs, confidence);
    }

    private Distribution parseDistribution(JSONObject dist, Rvgs rvg) {
        if(dist.containsKey(expoDistKey)) {
            JSONObject innerDist = (JSONObject) dist.get(expoDistKey);
            Double mean = (Double) innerDist.get(meanKey);
            if(mean == null) {
                throw new IllegalArgumentException("Mean must be defined for the exponential distribution");
            }
            return Exponential.createExponential(rvg, mean);
        } else {
            if(dist.containsKey(normalDistKey)) {
                JSONObject innerDist = (JSONObject) dist.get(normalDistKey);
                Double mean = (Double) innerDist.get(meanKey);
                Double sigma = (Double) innerDist.get(sigmaKey);
                if(mean == null || sigma == null) {
                    throw new IllegalArgumentException("Mean and sigma must be defined for the normal distribution");
                }
                return Normal.createNormal(rvg, mean, sigma);
            } else {
                if(dist.containsKey(uniformDistKey)) {
                    JSONObject innerDist = (JSONObject) dist.get(uniformDistKey);
                    Double min = (Double) innerDist.get(minKey);
                    Double max = (Double) innerDist.get(maxKey);
                    if(min == null || max == null) {
                        throw new IllegalArgumentException("Min and max must be defined for the uniform distribution");
                    }
                    return createUniform(rvg, min, max);
                } else {
                    if(dist.containsKey(hyperDistKey)) {
                        JSONObject innerDist = (JSONObject) dist.get(hyperDistKey);
                        Double mean1 = (Double) innerDist.get(mean1Key);
                        Double mean2 = (Double) innerDist.get(mean2Key);
                        Double p = (Double) innerDist.get(pKey);
                        if(mean1 == null || mean2 == null || p == null) {
                            throw new IllegalArgumentException("Two mean values and a probability must be specified for the hyperexponential distribution");
                        }
                        return Hyperexponential.createHyperexponential(rvg, p, mean1, mean2);
                    } else {
                        if(dist.containsKey(erlangDistKey)) {
                            JSONObject innerDist = (JSONObject) dist.get(erlangDistKey);
                            Long k = (Long) innerDist.get(kKey);
                            Double mean = (Double) innerDist.get(meanKey);
                            if(k == null || mean == null) {
                                throw new IllegalArgumentException("The parameters k and mean must be specified for the erlang distribution");
                            }
                            return Erlang.createErlang(rvg, k.intValue(), mean);
                        } else {
                            throw new IllegalArgumentException("No distribution specified");
                        }
                    }
                }
            }
        }
    }
}
