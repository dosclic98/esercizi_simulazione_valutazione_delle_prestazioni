package SimUtils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class ConfigReaderM3 extends ConfigReaderM1M2{
    private static final String pKey = "p";

    public ConfigReaderM3(String fileName) {
        super(fileName);
    }

    public SimConfigM3 readConfig() throws IOException, ParseException {
        SimConfigM1M2 partialConfig = super.readConfig();

        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(configFilePath + fileName));
        Double p = (Double) jsonObject.get(pKey);

        if(p == null) {
            throw new IllegalArgumentException("You must specify the routing probability p");
        }
        if(p <= 0 || p > 1) {
            throw new IllegalArgumentException("The routing probability must be between 0 (exclusive) and 1 (inclusive)");
        }

        return new SimConfigM3(partialConfig, p, seed);
    }

}
