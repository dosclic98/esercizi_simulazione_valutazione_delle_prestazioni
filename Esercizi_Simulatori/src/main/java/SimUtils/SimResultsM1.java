package SimUtils;

public class SimResultsM1 {
    public double meanInterArrivalTime;
    public double meanServiceTime;
    public double[] serverBusy;
    public double totalBusy;
    public double avgServersBusy;
    public double avgQueueLength;
    public long maxQueueLength;
    public double throughput;
    public double avgResTime;
    public double simTotTime;
    public long totalCustomers;

    public SimResultsM1(
            double meanInterArrivalTime,
            double meanServiceTime,
            double[] serverBusy,
            double totalBusy,
            double avgServersBusy,
            double avgQueueLength,
            long maxQueueLength,
            double throughput,
            double avgResTime,
            double simTotTime,
            long totalCustomers) {
        this.meanInterArrivalTime = meanInterArrivalTime;
        this.meanServiceTime = meanServiceTime;
        this.serverBusy = serverBusy;
        this.totalBusy = totalBusy;
        this.avgServersBusy = avgServersBusy;
        this.avgQueueLength = avgQueueLength;
        this.maxQueueLength = maxQueueLength;
        this.throughput = throughput;
        this.avgResTime = avgResTime;
        this.simTotTime = simTotTime;
        this.totalCustomers = totalCustomers;
    }

}
