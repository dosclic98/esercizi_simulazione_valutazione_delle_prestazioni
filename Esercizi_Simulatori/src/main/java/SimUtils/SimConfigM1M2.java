package SimUtils;

import distributions.Distribution;

public class SimConfigM1M2 {
    public Distribution arrivalDist;
    public Distribution serviceDist;
    public Long customers;
    public Long servers;
    public Long runs;
    public Double confidence;

    public SimConfigM1M2(Distribution arrivalDist, Distribution serviceDist,
                         Long customers, Long servers, Long runs, Double confidence) {
        this.arrivalDist = arrivalDist;
        this.serviceDist = serviceDist;
        this.customers = customers;
        this.servers = servers;
        this.runs = runs;
        this.confidence = confidence;
    }

}
