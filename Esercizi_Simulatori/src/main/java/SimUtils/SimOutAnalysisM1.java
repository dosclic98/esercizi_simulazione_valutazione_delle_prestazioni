 package SimUtils;

import java.util.ArrayList;

public class SimOutAnalysisM1 extends SimOutAnalysisTemplate{
    private final ArrayList<SimResultsM1> simResults;

    public SimOutAnalysisM1(ArrayList<SimResultsM1> simResults, Double confidence, Long numRuns) {
        this.numRuns = numRuns;
        this.simResults = simResults;
        this.confidInterval = confidence;
    }

    public void doAnalysis() {
        ArrayList<Double> meanInterArrivalTimes = new ArrayList<>();
        ArrayList<Double> meanServiceTimes = new ArrayList<>();
        ArrayList<Double> totalBusyList = new ArrayList<>();
        ArrayList<Double> avgServerBusyList = new ArrayList<>();
        ArrayList<Double> avgQueueLengthList = new ArrayList<>();
        ArrayList<Long> maxQueueLengthList = new ArrayList<>();
        ArrayList<Double> throughputList = new ArrayList<>();
        ArrayList<Double> avgResTimeList = new ArrayList<>();
        ArrayList<Double> simTotTimeList = new ArrayList<>();

        ArrayList<ArrayList<Double>> serverBusyList = new ArrayList<>();
        for(int j = 0; j < simResults.get(0).serverBusy.length; j++) {
            serverBusyList.add(new ArrayList<>());
        }

        for(SimResultsM1 res : simResults) {
            for(int i = 0; i < res.serverBusy.length; i++) {
                serverBusyList.get(i).add(res.serverBusy[i]);
            }
            meanInterArrivalTimes.add(res.meanInterArrivalTime);
            meanServiceTimes.add(res.meanServiceTime);
            totalBusyList.add(res.totalBusy);
            avgServerBusyList.add(res.avgServersBusy);
            avgQueueLengthList.add(res.avgQueueLength);
            maxQueueLengthList.add(res.maxQueueLength);
            throughputList.add(res.throughput);
            avgResTimeList.add(res.avgResTime);
            simTotTimeList.add(res.simTotTime);
        }
        System.out.println();
        doubleEstimation(meanInterArrivalTimes, "#### Mean Inter-arrival times ####");
        doubleEstimation(meanServiceTimes, "#### Mean Service times ####");
        for(int k = 0; k < serverBusyList.size(); k++) {
            ArrayList<Double> serverBusy = serverBusyList.get(k);
            doubleEstimation(serverBusy, "#### Server " + (k+1) + " utilization ####");
        }
        doubleEstimation(totalBusyList, "#### Total busy ####");
        doubleEstimation(avgServerBusyList, "#### Average servers busy ####");
        doubleEstimation(avgQueueLengthList, "#### Average queue length ####");
        longEstimation(maxQueueLengthList, "#### Max queue length ####");
        doubleEstimation(throughputList, "#### Throughput ####");
        doubleEstimation(avgResTimeList, "#### Average response time ####");
        doubleEstimation(simTotTimeList, "#### Sim total time ####");
    }
}
