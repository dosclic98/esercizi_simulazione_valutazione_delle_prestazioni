package SimUtils;

public class SimResultsM3 {
    public double meanDelayTime;
    public double meanServiceTime1;
    public double meanServiceTime2;
    public double[] serverBusy1;
    public double[] serverBusy2;
    public double totalBusy1;
    public double totalBusy2;
    public double avgServersBusy1;
    public double avgServersBusy2;
    public double avgQueueLength1;
    public double avgQueueLength2;
    public long maxQueueLength1;
    public long maxQueueLength2;
    public double avgCliStation;
    public double throughput1;
    public double throughput2;
    public double avgResTime1;
    public double avgResTime2;
    public double simTotTime;
    public long totalCustomers;
    public double probIneff;

    public SimResultsM3(
            double meanDelayTime,
            double meanServiceTime1,
            double meanServiceTime2,
            double[] serverBusy1,
            double[] serverBusy2,
            double totalBusy1,
            double totalBusy2,
            double avgServersBusy1,
            double avgServersBusy2,
            double avgQueueLength1,
            double avgQueueLength2,
            long maxQueueLength1,
            long maxQueueLength2,
            double avgCliStation,
            double throughput1,
            double throughput2,
            double avgResTime1,
            double avgResTime2,
            double simTotTime,
            long totalCustomers,
            double probIneff) {
        this.meanDelayTime = meanDelayTime;
        this.meanServiceTime1 = meanServiceTime1;
        this.meanServiceTime2 = meanServiceTime2;
        this.serverBusy1 = serverBusy1;
        this.serverBusy2 = serverBusy2;
        this.totalBusy1 = totalBusy1;
        this.totalBusy2 = totalBusy2;
        this.avgServersBusy1 = avgServersBusy1;
        this.avgServersBusy2 = avgServersBusy2;
        this.avgQueueLength1 = avgQueueLength1;
        this.avgQueueLength2 = avgQueueLength2;
        this.maxQueueLength1 = maxQueueLength1;
        this.maxQueueLength2 = maxQueueLength2;
        this.avgCliStation = avgCliStation;
        this.throughput1 = throughput1;
        this.throughput2 = throughput2;
        this.avgResTime1 = avgResTime1;
        this.avgResTime2 = avgResTime2;
        this.simTotTime = simTotTime;
        this.totalCustomers = totalCustomers;
        this.probIneff = probIneff;
    }

}
