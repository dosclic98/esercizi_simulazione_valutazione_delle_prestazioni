package SimUtils;

import java.util.ArrayList;

public class SimOutAnalysisTemplate {
    public Double confidInterval = 95.0;
    public Long numRuns;


    public void doubleEstimation(ArrayList<Double> list, String msg) {
        System.out.println(msg);
        System.out.println("Expected value estimation: " + calculateAvgDouble(list));
        System.out.print("Inerval estimation: ");
        intervalEstimationDouble(list);
        System.out.println();
    }

    public void longEstimation(ArrayList<Long> list, String msg) {
        System.out.println(msg);
        System.out.println("Expected value estimation: " + calculateAvgLong(list));
        System.out.print("Inerval estimation: ");
        intervalEstimationLong(list);
        System.out.println();
    }

    private void intervalEstimationDouble(ArrayList<Double> list) {
        assert(numRuns == list.size());
        double low = calculateAvgDouble(list) -
                (calculateInverseTStudent(numRuns-1, calculateProbability(confidInterval)) * calculateSigmaDouble(list));
        double high = calculateAvgDouble(list) +
                (calculateInverseTStudent(numRuns-1, calculateProbability(confidInterval)) * calculateSigmaDouble(list));
        System.out.println(low + " <= E(U) <= " + high);
        double avg = (high - low) / 2;
        double accuracy = (avg * 100) / (high - avg);
        System.out.println("Accuracy: " + accuracy + " %");
    }

    private void intervalEstimationLong(ArrayList<Long> list) {
        double low = calculateAvgLong(list) -
                (calculateInverseTStudent(numRuns - 1, calculateProbability(confidInterval)) * calculateSigmaLong(list));
        double high = calculateAvgLong(list) +
                (calculateInverseTStudent(numRuns - 1, calculateProbability(confidInterval)) * calculateSigmaLong(list));
        System.out.println(low + " <= E(U) <= " + high);
        double avg = (high - low) / 2;
        double accuracy = (avg * 100) / (high - avg);
        System.out.println("Accuracy: " + accuracy + " %");
    }

    private double calculateProbability(double confidInterval) {
        return ((100 - confidInterval) / 2.0) / 100;
    }

    private double calculateInverseTStudent(double dof, double probability) {
        return Math.abs(new org.apache.commons.math3.distribution.TDistribution(dof).
                inverseCumulativeProbability(probability));
    }

    private double calculateAvgDouble(ArrayList<Double> params) {
        double sumParams = 0;
        if(params == null || params.isEmpty()) return 0;
        for(int i = 0; i < params.size(); i++) {
            if(i == 0) sumParams = params.get(i);
            else sumParams += params.get(i);
        }
        return sumParams / params.size();
    }

    private double calculateAvgLong(ArrayList<Long> params) {
        long sumParams = 0;
        if(params == null || params.isEmpty()) return 0;
        for(int i = 0; i < params.size(); i++) {
            if(i == 0) sumParams = params.get(i);
            else sumParams += params.get(i);
        }
        return (double) sumParams / params.size();
    }

    private double calculateSigmaDouble(ArrayList<Double> params) {
        double sumParams = 0;
        double avg = calculateAvgDouble(params);
        if(params == null || params.isEmpty()) return 0;
        for(int i = 0; i < params.size(); i++) {
            if(i == 0) sumParams = Math.pow(params.get(i) - avg, 2);
            else sumParams += Math.pow(params.get(i) - avg, 2);
        }
        return Math.sqrt((sumParams / (params.size() - 1)) / params.size());
    }

    private double calculateSigmaLong(ArrayList<Long> params) {
        double sumParams = 0;
        double avg = calculateAvgLong(params);
        if(params == null || params.isEmpty()) return 0;
        for(int i = 0; i < params.size(); i++) {
            if(i == 0) sumParams = Math.pow(params.get(i) - avg, 2);
            else sumParams += Math.pow(params.get(i) - avg, 2);
        }
        return Math.sqrt((sumParams / (params.size() - 1)) / params.size());
    }

    private double estimateNumRuns(double probability, long prevNumRuns, double S0, double targetAcc) {
        return (Math.pow(calculateInverseTStudent(prevNumRuns, probability), 2) * S0) / Math.pow(targetAcc,2);
    }
}
