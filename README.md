# Esercizi_Simulazione_Valutazione_delle_Prestazioni
Repository contenente gli esercizi pratici dell'esame di Valutazione delle Prestazioni

# Compilazione ed esecuzione
1) Recarsi nella cartella "Esercizi_Simulatori"
2) Eseguire la compilazione con il comando "mvn package"
3) Restando nella cartella corrente è possibile eseguire il 
file jar generato nella cartella target con il comando 
	java -jar target/Esercizi_Simimulatori-1.0-SNAPSHOT.jar start
	
	Es. 
	java -jar target/Esercizi_Simimulatori-1.0-SNAPSHOT.jar start --m=1 --cfg=model1Tester.json
	java -jar target/Esercizi_Simimulatori-1.0-SNAPSHOT.jar start --m=2 --cfg=model2Tester.json
